﻿using UnityEngine;
using System.Collections;
using Devdog.InventorySystem;
using Devdog.InventorySystem.Models;
using Devdog.InventorySystem.UnityStandardAssets;
using System;
using System.Collections.Generic;

public class CharStatController : MonoBehaviour {

    private PlayerModel playerModel;
    private PlayerController playerController;
    private InventoryPlayer inventoryPlayer;


	// Use this for initialization
	void Start () {
        playerModel = GetComponent<PlayerModel>();
        playerController = GetComponent<PlayerController>();

        inventoryPlayer = GetComponent<InventoryPlayer>();

        inventoryPlayer.characterCollection.stats.OnStatChanged += CharacterCollectionOnOnStatChanged;
        inventoryPlayer.characterCollection.OnAddedItem += CharacterCollectionItemChanged;
        inventoryPlayer.characterCollection.OnRemovedItem += CharacterCollectionItemRemoved;
    }

    private void CharacterCollectionItemRemoved(InventoryItemBase item, uint itemID, uint slot, uint amount)
    {
        if (item.tag == "melee")
            playerModel.meleeWeapon = null;
        else if (item.tag == "ranged")
            playerModel.rangedWeapon = null;
        else if (item.tag == "shield")
            playerModel.shield = null;
    }

    private void CharacterCollectionItemChanged(IEnumerable<InventoryItemBase> items, uint amount, bool cameFromCollection)
    {
        foreach (var item in items)
        {
            if (item.tag == "melee")
                playerModel.meleeWeapon = item.GetComponent<MeleeWeapon>();
            else if (item.tag == "ranged")
                playerModel.rangedWeapon = item.GetComponent<RangedWeapon>();
            else if (item.tag == "shield")
                playerModel.shield = item.GetComponent<Weapon>();

            Debug.Log(playerModel.meleeWeapon + " " + playerModel.rangedWeapon + " " + playerModel.shield);
        }
    }

    private void CharacterCollectionOnOnStatChanged(IInventoryCharacterStat stat)
    {
        if (stat.statName == "Movement Speed")
        {
            playerModel.speed = stat.currentValue;
        }

        if (stat.statName == "Agility")
        {
            //playerModel.speed = stat.currentValue;
        }
        if (stat.statName == "Melee Damage")
        {
            //playerModel.speed = stat.currentValue;
        }

        if (stat.statName == "Ranged Damage")
        {
            /*playerModel.speed = stat.currentValue*/;
        }

        if (stat.statName == "Defense")
        {
            playerModel.defense = stat.currentValue;
        }


        if (stat.statName == "Health")
        {
            playerModel.health = stat.currentValue;
            playerController.health.MaxVal = stat.currentValue;
            playerController.health.CurrentVal = stat.currentValue;
        }

        if (stat.statName == "Mana")
        {
            playerModel.mana = stat.currentValue;
            playerController.mana.MaxVal = stat.currentValue;
            playerController.mana.CurrentVal = stat.currentValue;
        }

        if (stat.statName == "Stamina")
        {
            playerModel.stamina = stat.currentValue;
            playerController.stamina.MaxVal = stat.currentValue;
            playerController.stamina.CurrentVal = stat.currentValue;
        }

        if (stat.statName == "Mana Regen Rate")
        {
            playerModel.manaRegenRate = stat.currentValue;
            playerController.mana.regenRate = stat.currentValue;
        }

        if (stat.statName == "Health Regen Rate")
        {
            playerModel.manaRegenRate = stat.currentValue;
            playerController.health.regenRate = stat.currentValue;
        }

        if (stat.statName == "Stamina Regen Rate")
        {
            playerModel.manaRegenRate = stat.currentValue;
            playerController.stamina.regenRate = stat.currentValue;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
