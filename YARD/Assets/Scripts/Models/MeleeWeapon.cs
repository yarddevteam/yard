﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class MeleeWeapon : Weapon
{
    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").gameObject;
        boxCollider = player.GetComponent<PlayerController>().meleeRange.GetComponent<BoxCollider2D>();
        Hide();
    }

    public override void UseInDirctionOfMouseClick()
    {
        Vector3 parentPos = player.transform.position;
        Animator anim = player.GetComponent<PlayerController>().meleeAnim.GetComponent<Animator>();
        Vector3 screenMousePos = Input.mousePosition;
        screenMousePos.z = 10;
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(screenMousePos);
        Vector3 mouseDisplacement = mousePos - parentPos;
        AudioController.instance.playSoundEffect(AudioController.SE_ATTACK);
        if (Math.Abs(mouseDisplacement.x) > Math.Abs(mouseDisplacement.y))
        {
            if (mousePos.x < parentPos.x)
            {
                if (this is Weapon)
                {
                    boxCollider.size = horizontalSize;
                    boxCollider.offset = new Vector2(horizontalOffsets.x, 0);
                    boxCollider.enabled = true;
                    anim.Play(leftAtack);
                    //anim.CrossFade(leftAtack, 0.3f);
                }
            }
            else if (mousePos.x > parentPos.x)
            {
                if (this is Weapon)
                {
                    boxCollider.size = horizontalSize;
                    boxCollider.offset = new Vector2(horizontalOffsets.y, 0);
                    boxCollider.enabled = true;
                    //anim.Play(rightAttack);
                    anim.CrossFade(rightAttack, 0.3f);
                }
            }
        }
        else
        {
            if (mousePos.y > parentPos.y)
            {
                if (this is Weapon)
                {
                    boxCollider.size = verticalSize;
                    boxCollider.offset = new Vector2(0, verticalOffsets.x);
                    boxCollider.enabled = true;
                    Debug.Log(upAttack);
                    //player.GetComponent<Animation>()[upAttack].wrapMode = WrapMode.Once;
                    anim.CrossFade(upAttack, 0.3f);
                }
            }
            else if (mousePos.y < parentPos.y)
            {
                if (this is Weapon)
                {
                    boxCollider.size = verticalSize;
                    boxCollider.offset = new Vector2(0, verticalOffsets.y);
                    boxCollider.enabled = true;
                    anim.CrossFade(downAttack, 0.3f);
                }
            }
        }
    }
}

