﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharController : EntityController
{
    public Animator animator;
    public CharacterMotionState motionState = new CharacterMotionState();

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void MoveTo(Vector3 velocity)
    {
        transform.Translate(velocity * Time.deltaTime);
    }


    public void StartAttack()
    {        

    }
}

