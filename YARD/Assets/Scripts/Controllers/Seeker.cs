﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Seeker : MonoBehaviour
{
    public int currentWaypoint = 0;
    public float nextWaypointDistance = 1f;
    public bool isSeeking = false;
    public Vector3? lastTargetPos = Vector3.zero;
    public List<Vector3> path = new List<Vector3>();

    internal void StartPath(Vector3 position)
    {
        if ( lastTargetPos != null)
        {
            //path = Environment.GetPath(position, (Vector3) lastTargetPos);
        }

        if (path.Count != 0)
        {
            isSeeking = true;
            currentWaypoint = 0;
        }
    }

    internal void Reset()
    {
        currentWaypoint = 0;
        isSeeking = false;
        lastTargetPos = null;
        path.Clear();
        //Debug.Log("Seeker reset");
    }

    void OnDrawGizmos()
    {
        //if (path != null && path.Count > 0)
        //{
        //    foreach(Vector3 v in path)
        //    {
        //        Gizmos.DrawLine(transform.position, v);
        //        Gizmos.DrawCube(v + new Vector3(0.5f, -0.5f), v);
        //    }
        //}
    }
}