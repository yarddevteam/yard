﻿
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharacterModel : EntityModel
{
    public Vector3 velocity = Vector3.zero;
    public Vector3 direction = Vector3.zero;
    public AudioSource walkSound, deathSound, hitSound;
    public bool isAttacking = false;
    public float mana = 5,
        stamina = 5,
        maxHealth = 10,
        maxMana = 5,
        maxStamina = 5,
        dodge = 0,
        sprint = 1.2f,
        defense = 1,
        speed = 1f;
}

