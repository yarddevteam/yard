﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Weapon : Item
{
    public static event EventHandler WeaponKill;
    public BoxCollider2D boxCollider;
    public Vector2 horizontalOffsets, horizontalSize, verticalOffsets, verticalSize;
    public string upAttack, downAttack, rightAttack, leftAtack;
    public bool inUse = false;
    public int knockback = 1;
    public float useTime,
        timeInUse,
        defaultDamage = 1f,
        damage = 1f,
        speed = 1f;

    public void CheckUsage()
    {
        if (inUse)  
        {
            if ((timeInUse += Time.deltaTime) > useTime)
            {
                Hide();
                timeInUse = 0f;
            }
        }
    }

    public void Use()
    {
        if (!inUse)
        {
            inUse = true;
        }

        UseInDirctionOfMouseClick();
    }

    public virtual void UseInDirctionOfMouseClick(){}

    public void Hide()
    {
        inUse = false;
        //sprite.enabled = false;
        boxCollider.enabled = false;
        boxCollider.size = new Vector2(1, 1);
        boxCollider.offset = new Vector2(0, 0);
    }

    public virtual void OnWeaponKill()
    {
        if (WeaponKill != null)
            WeaponKill(this, new EventArgs());
    }
}

