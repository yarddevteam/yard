﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Item : MonoBehaviour
{
    public GameObject prefab;
    public GameObject player;
    public SpriteRenderer sprite;
    public float attractionRadius = 20f;
    public float attractionMagnitude = 20f;

    void Update()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player").gameObject;

        var r = player.transform.position - transform.position;
        if ( r.magnitude < attractionRadius) 
        {
            GetComponent<Rigidbody2D>().AddForce( r.normalized * attractionMagnitude * ( 1 / r.sqrMagnitude ) );
        }
    }
}
[Serializable]
public class Comsumable : Item
{

}






