﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[Serializable]
class PlayerController : CharController
{
    public PlayerModel player;
    public GameObject meleeRange;
    public GameObject meleeAnim;
    public Stat health;
    public Stat mana;
    public Stat stamina;

    void Start()
    {
        player = GetComponent<PlayerModel>();
        player.rigidBody2D = GetComponent<Rigidbody2D>();
        player.boxCollider2D = GetComponent<BoxCollider2D>();

        HudBar[] bars = GameObject.Find("HUD").GetComponentsInChildren<HudBar>();
        
        foreach (HudBar bar in bars)
        {
            if (bar.name == "HealthBar")
            {
                health = new Stat();
                health.bar = bar;
                health.Init(player.maxHealth, player.healthRegenRate);
            }

            if (bar.name == "ManaBar")
            {
                mana = new Stat();
                mana.bar = bar;
                mana.Init(player.maxMana, player.manaRegenRate);
            }

            if (bar.name == "StaminaBar")
            {
                stamina = new Stat();
                stamina.bar = bar;
                stamina.Init(player.maxStamina, player.staminaRegenRate, player.staminaUseageRate);
            }
        }

    }

    void Update()
    {
        if (player.meleeWeapon != null)
        {
            player.meleeWeapon.CheckUsage();
        }

        if (Time.timeScale == 0f)
        {
            return;
        }

        float scroll;

        if( Input.GetMouseButtonDown(0))
            UseMelee();

        if( Input.GetMouseButtonDown(1))
            UseRange();
         else if (Input.GetMouseButton(1))
            UseRange();

        if( Input.GetMouseButtonDown(2))
            UseShield();

        //if( (scroll = Input.GetAxis("Mouse ScrollWheel")) != 0 )
        //    CycleQuickBar();
    }

    void FixedUpdate()
    {
        player.direction.x = Input.GetAxisRaw("Horizontal");
        player.direction.y = Input.GetAxisRaw("Vertical");

        if (Input.GetKey("space") && !(stamina.CurrentVal <= 0.2f))
        {
            player.velocity = player.direction * player.speed * player.sprint;
            stamina.CurrentVal -= stamina.usageRate * Time.deltaTime;
        }
        else
            player.velocity = player.direction * player.speed;

        
        stamina.Regen();

        mana.Regen();
        health.Regen();
        MoveTo(player.velocity);
    }

    private void CycleQuickBar()
    {
        throw new NotImplementedException();
    }

    private void UseShield()
    {
        throw new NotImplementedException();
    }

    private void UseRange()
    {
        if (player.rangedWeapon)
        {
            Debug.Log(mana.CurrentVal);
            if (mana.CurrentVal < player.rangedWeapon.manaCost)
            {
                Debug.Log("Out of mana");
                return;
            }
            else
            {
                Debug.Log("FIre");
                mana.CurrentVal -= player.rangedWeapon.manaCost;
                player.rangedWeapon.Use();

            }

        }
    }

    private void UseMelee()
    {
        if (player.meleeWeapon)
        {
            GetComponentInChildren<DamageDecter>().weapon = player.meleeWeapon;
            player.meleeWeapon.Use();
        }
    }

    public override void TakeDamage(float damage, Vector2 knockback)
    {
        player.rigidBody2D.AddForce(knockback);
        health.CurrentVal -= damage;

        if (health.CurrentVal <= 0.05f)
        {
            Game.Reset();
            SceneManager.LoadScene(4);
        }
    }
}

