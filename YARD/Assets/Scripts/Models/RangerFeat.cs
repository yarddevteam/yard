﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class RangerFeat : Feat
{
    void Start()
    {
        sequentialAction = typeof(RangedWeapon);
        Weapon.WeaponKill += HandleWeaponKill;
        actionsToCompleteSeq = 10;
    }

    private void HandleWeaponKill(object sender, EventArgs e)
    {
        SequentialAction(sender.GetType());
    }
}

