﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Game : MonoBehaviour
{
    public static Game currentGame;
    public static int currentLevel = 0;
    public static bool levelComplete = false;

    public List<PlayerProfile> profiles = new List<PlayerProfile>();
    public PlayerProfile selectedProfile;
    public GameObject env;
    public GameObject player;
    public GameObject gameMenu;
    public Object[] sceneObjects;
    public static int maxLevel = 10;

    static float startTime;
    public static float timeLimit = 1200;

    //Feat Tracking Stats
    static bool discoveredAllTiles = true;


    void Awake()
    {
        //singleton pattern
        if (currentGame == null)
        {
            DontDestroyOnLoad(gameObject);
            currentGame = this;
        }
        else if (currentGame != this)
        {
            Destroy(gameObject);
        }

        env = GameObject.FindGameObjectWithTag("Environment");
        player = GameObject.FindGameObjectWithTag("Player");
        SaveLoad.LoadProfileList();
        startTime = Time.time;
    }

    void Start()
    {
    }

    void Update()
    {
        if (player && !AudioController.instance.soundTracks.isPlaying)
        {
            AudioController.instance.playSoundTrack(AudioController.ST_LEVEL);
        }
        else if (GameObject.FindGameObjectWithTag("Player") != null && !AudioController.instance.soundTracks.isPlaying)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            AudioController.instance.playSoundTrack(AudioController.ST_LEVEL);
        }
        else if (!AudioController.instance.soundTracks.isPlaying )
            AudioController.instance.playSoundTrack(AudioController.ST_MAIN_MENU);
    }

    public static void CheckExplorerAchievment() {
        GameObject env = GameObject.FindGameObjectWithTag("Environment");
        List<Tile> tiles = env.GetComponentsInChildren<Tile>().ToList();
        List<Tile> discoveredTiles = tiles.Where(t => t.discovered == true).ToList();
        List<Tile> hiddenTiles = tiles.Where(t => t.discovered == false && t.type == Environment.TileType.Floor).ToList();
        if (hiddenTiles.Count > 0) {
            discoveredAllTiles = false;
        }
    }

    public static void CheckSpeedRunnerAchievment() {
        if (currentLevel == maxLevel) {
            float delta = Time.time - startTime;
            if (delta < timeLimit) {
                //Debug.Log("ACHIEVMENT GET: SPEEDRUNNER");
            }
        }
    }

    public static void SaveSession()
    {
        //SaveLoad.Save(Game.currentGame);
    }

    public static void Reset()
    {
        currentLevel = 0;
    }
}