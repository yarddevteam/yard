﻿using System;
using System.Collections.Generic;

[Serializable]
public class PlayerProfile
{
    public string name;
    public int id;
    public Dictionary<string, bool> achievements = new Dictionary<string, bool>();
    //public Gamesave recentSave;
    private string text;

    public PlayerProfile(string name, int id)
    {
        this.name = name;
        this.id = id;
    }
}
