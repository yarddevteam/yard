﻿using UnityEngine;
using System.Collections;

public class DamageDecter : MonoBehaviour {
    public Weapon weapon;
    public Collider2D collider;
    public GameObject text;

    // Use this for initialization
    void Start () {
        collider = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Vector2 direction = (other.transform.position - transform.position).normalized;
        EnemyController victim = other.GetComponent<EnemyController>();

        if ( victim != null)
        {
            victim.TakeDamage(weapon.damage, weapon.knockback * direction);
            Vector3 v = Camera.main.WorldToViewportPoint(other.transform.position);
            //DmgPoints.SpawnText(weapon.damage, v.x, v.y, text);
            if (!victim.model.isAlive)
            {
                weapon.OnWeaponKill();
            }

        }
    }
}
