﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class Tile : EntityModel
{
    public Tile parent;
    public Environment.TileType type;
    public bool isWalkable = true;
    public bool discovered = false;
    public Color defaultColor;
    private Color currentColor;
    private Color targetColor;
    public bool changingColor;
    public float changeSpeed = 0.25f;
    public Color changeTolerance = new Color(0.05f, 0.05f, 0.05f);

    public Color color
    {
        get
        {
            return currentColor;
        }

        set
        {
            targetColor = value;
            changingColor = true;
        }
    }

    void Start()
    {
        defaultColor = GetComponent<SpriteRenderer>().color;
        currentColor = defaultColor;
    }

    void Update()
    {
        if (changingColor)
        {
            currentColor = Color.Lerp(currentColor, targetColor, changeSpeed * Time.deltaTime);
            GetComponent<SpriteRenderer>().color = currentColor;
            if (currentColor ==  targetColor)
            {
                changingColor = false;
            }
        }
    }

    public int f {
        get
        {
            return g + h;
        }
         
        set {}
    }

    public int g { get; set; }
    public int h { get; set; }

    public void RecalculateG()
    {
        Tile t = parent;
        g = 0;

        while( t.g != 0 )
        {
            g += t.g;
            t = t.parent;
        }
    }

    public override string ToString()
    {
        return "Tile: position (" + transform.position.x + " , " + transform.position.y + "), isWalkable: " + isWalkable;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            discovered = true;
    }

    public void ResetColor()
    {
        GetComponent<SpriteRenderer>().color = defaultColor;
    }
}

