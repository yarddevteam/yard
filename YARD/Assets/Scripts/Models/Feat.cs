﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Feat : MonoBehaviour
{
    public int tier = 0;

    // Sequential related shit
    public Type sequentialAction; // Defined in child
    public int actionsToCompleteSeq = 0; // Defined in child
    public int actionsCompletedInSeq = 0;

    // Time related shit
    public int actionsToCompleteTime = 0; // Defined in child
    public float actionTimeLimit = 0; // Defined in child
    public int actionsCompletedInTime = 0;
    public float actionTimer = 0;

    void Update()
    {

        // Sequence Feat
        if ( actionsToCompleteSeq > 0 )
        {
            if (actionsCompletedInSeq >= actionsToCompleteSeq)
            {
                tier++;
                actionsCompletedInSeq = 0;
            }
        }

        // Time Feat
        if ( actionsToCompleteTime > 0 )
        {
            actionTimer += Time.deltaTime;
            if (actionTimer > actionTimeLimit)
            {
                actionTimer = 0;
                actionsCompletedInTime = 0;

                if (actionsCompletedInTime >= actionsToCompleteTime) 
                    tier++;
            }
        }
    }

    protected void SequentialAction(Type action)
    {
        if (action == sequentialAction)
        {
            actionsCompletedInSeq++;
        }
        else {
            actionsCompletedInSeq = 0;
        }
    }

    protected void TimedAction(Action action)
    {
        actionsCompletedInTime++;
    }
}


