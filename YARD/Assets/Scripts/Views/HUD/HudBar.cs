﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class HudBar : MonoBehaviour {
	
	private float fillAmount;

	[SerializeField]
	private Image content;

	[SerializeField]
	private Text valueText;

    [SerializeField]
    private float lerpSpeed;

	public float MaxValue { get; set; }

	public float Value{
		set{
			string[] tmp = valueText.text.Split (':');
			valueText.text = tmp[0] + ": " + (int) value;
            fillAmount = value / MaxValue;
		}

	}

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		HandleBar ();
	}

	private void HandleBar()
    {
		if (fillAmount != content.fillAmount) {
			content.fillAmount = Mathf.Lerp (content.fillAmount, fillAmount,Time.deltaTime * lerpSpeed);
		}
	}
}
