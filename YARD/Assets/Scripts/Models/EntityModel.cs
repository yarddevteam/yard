﻿using System;
using UnityEngine;

[Serializable]
public class EntityModel : MonoBehaviour
{
    public Rigidbody2D rigidBody2D;
    public BoxCollider2D boxCollider2D;
    public AnimationCurve slopeSpeedMultiplier;
    public float health = 10f;
}

