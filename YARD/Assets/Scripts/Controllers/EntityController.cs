﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class EntityController : MonoBehaviour
{
    public virtual void TakeDamage(float damage, Vector2 knockback)
    {
        Debug.Log(this.name);
        Debug.Log("called 'TakeDamage' without providing its own definition.");
    }
}

