﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DmgPoints : MonoBehaviour {
    public Color color = new Color(0.8f, 0.8f, 0f, 1f);
    public float scroll = 0.05f;
    public float duration = 1.5f;
    public float alpha;

	// Use this for initialization
	void Start () {
        GetComponent<Text>().color = color;
        alpha = 1f;
	}
	
	// Update is called once per frame
	void Update () {
	    if( alpha > 0 )
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - scroll * Time.deltaTime);
            alpha -= Time.deltaTime / duration;
            GetComponent<Text>().color = new Color(color.r, color.g, color.b, alpha);
        }
        else
        {
            Destroy(gameObject);
        }
	}

    public static void SpawnText(float points, float x, float y, GameObject prefab)
    {
        x = Mathf.Clamp(x, 0.05f, 0.095f);
        y = Mathf.Clamp(y, 0.05f, 0.9f);
        GameObject text = Instantiate(prefab, new Vector3(x, y), Quaternion.identity) as GameObject;
        text.GetComponent<Text>().text = points.ToString();
    }
}
