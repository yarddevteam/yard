﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapController : MonoBehaviour {

    public bool isShown = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void OnMap()
    {
        GameObject env = GameObject.FindGameObjectWithTag("Environment");
        List<Tile> tiles = env.GetComponentsInChildren<Tile>().ToList();
        List<Tile> discovredTiles = tiles.Where(t => t.discovered == true).ToList();
        List<Tile> hiddenTiles = tiles.Where(t => t.discovered == false).ToList();

        if ( isShown )
        {
            isShown = false;
            Camera.main.GetComponent<CameraController>().ZoomInCamera();
            discovredTiles.ForEach(t => t.color = t.defaultColor);
            hiddenTiles.ForEach(t => t.color = t.defaultColor);
            return;
        }
        
        Camera.main.GetComponent<CameraController>().ZoomOutCamera();
        isShown = true;
        discovredTiles.ForEach(t => t.color = new Color(0, 0.21f, 1, 1));
        hiddenTiles.ForEach(t => t.color = Color.black);
    }
}
