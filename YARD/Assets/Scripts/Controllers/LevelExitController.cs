﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelExitController : MonoBehaviour {

    int step;

    // Use this for initialization
    void Start()
    {
        step = 0;
    }

    // Update is called once per frame
    void Update () {
        float scale = (float)step / 100;
        float offset = (1 - scale) / 2;
        float x = Mathf.Floor(transform.position.x) + offset;
        float y = Mathf.Floor(transform.position.y) + offset;

        transform.localScale = new Vector3(scale, scale, 1f);
        transform.position = new Vector3(x, y, transform.position.z);
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1 - scale);

        step = (step + 2) % 100;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("Player"))
            Game.levelComplete = true; 
    }
}
