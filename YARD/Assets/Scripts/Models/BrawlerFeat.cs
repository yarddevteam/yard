﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class BrawlerFeat : Feat
{
    void Start()
    {
        sequentialAction = typeof(MeleeWeapon);
        Weapon.WeaponKill += HandleWeaponKill;
        actionsToCompleteSeq = 10;
    }

    private void HandleWeaponKill(object sender, EventArgs e)
    {
        SequentialAction(sender.GetType());
    }
}

