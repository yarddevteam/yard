﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class Environment : MonoBehaviour
{
    // The type of tile that will be laid in a specific position.
    public enum TileType
    {
        Background, Floor, Wall, Exit
    }

    public GameObject player;
    public GameObject[] enemies;
    public GameObject[] item;

    //Level Generation Variables
    public int columns = 100;                          
    public int rows = 100;                                   
    public int minNumRooms = 2;
    public int maxNumRooms = 2;
    public int minRoomWidth = 5;
    public int maxRoomWidth = 10;
    public int minRoomHeight = 5;
    public int maxRoomHeight = 10;
    public int minCorrLength = 5;
    public int maxCorrLength = 10;
    public float enemySpawnChance = 0.4f;
    public float enemyAggression = 1f;
    public float enemyAggressionBuff = 1.2f;
    public float enemyHealthBuff = 1.2f;
    public IntRange numRooms;         
    public IntRange roomWidth;          
    public IntRange roomHeight;        
    public IntRange corridorLength;   
    public GameObject[] floorTiles;                           
    public GameObject[] wallTiles;
    public GameObject[] backgroundTiles;
    public GameObject[] outerWallTiles;                       
    public GameObject[] exitTiles;
    public TileType[][] tiles;
    private Room[] rooms;                                     
    private Corridor[] corridors;

    //Level label variables
    private bool showLevelLabel = false;
    public float labelDisplayTime = 3f;
    public float labelFadeoutTime = 2f;
    private float elapsedTime = 0f;
    private float labelAlpha = 1f;
    public string labelText = "Level ";
    public bool displayTimePassed = false;
    public bool fadeTimePassed = false;

    public static Dictionary<Point, Tile> nodes = new Dictionary<Point, Tile>();

    void Start()
    {
        //List<Tile> tiles = gameObject.GetComponentsInChildren<Tile>().ToList();
        numRooms = new IntRange(minNumRooms, maxNumRooms);
        roomWidth = new IntRange(minRoomWidth, maxRoomWidth);
        roomHeight = new IntRange(minRoomHeight, maxRoomHeight);
        corridorLength = new IntRange(minCorrLength, maxCorrLength);
        player = GameObject.FindGameObjectWithTag("Player");
        LoadFirstLevel();

        for (int i = 0; i < tiles.Length; i++)
        {
            for( int j = 0; j < tiles[i].Length; j++)
            {
                Point pair = new Point(i, j);

                if (nodes.ContainsKey(pair))
                {
                    Debug.Log("found duplication pair");
                    Debug.Log(pair);
                }
                else if (tiles[i][j] == TileType.Floor)
                {
                    //nodes.Add(pair, new Tile());
                }
            }
        }

    }

    void Update()
    {
        if (Game.levelComplete) {
            AdvanceLevel();
            Game.CheckExplorerAchievment();
            Game.CheckSpeedRunnerAchievment();
        }


        if (showLevelLabel)
        {
            elapsedTime += Time.deltaTime;
            if (!displayTimePassed)
            {
                if ( elapsedTime > labelDisplayTime)
                {
                    elapsedTime = 0;
                    displayTimePassed = true;
                }
            }
            else if (!fadeTimePassed)
            {
                labelAlpha = (labelFadeoutTime - elapsedTime) / labelFadeoutTime;
                if (elapsedTime > labelFadeoutTime)
                {
                    elapsedTime = 0;
                    fadeTimePassed = true;
                }
            }
            else
            {
                showLevelLabel = false;
            }
        }
    }

    private void LoadFirstLevel()
    {
        minNumRooms = 2;
        maxNumRooms = 2;
        minRoomWidth = 5;
        maxRoomWidth = 10;
        minRoomHeight = 5;
        maxRoomHeight = 10;
        minCorrLength = 5;
        maxCorrLength = 10;

        foreach (var enemy in enemies)
        {
            Enemy e = enemy.GetComponent<Enemy>();
            e.Reset();
        }

        LoadLevel();

        DestroyAll("Enemy");
        SpawnEntity(rooms[1], item[5], 1f);
        SpawnEntity(rooms[1], item[7], 1f);
    }

    private void LoadLevel()
    {
        numRooms = new IntRange(minNumRooms, maxNumRooms);
        roomWidth = new IntRange(minRoomWidth, maxRoomWidth);
        roomHeight = new IntRange(minRoomHeight, maxRoomHeight);
        corridorLength = new IntRange(minCorrLength, maxCorrLength);

        InitTiles();
        CreateRoomsAndCorridors();

        SetTilesValuesForRooms();
        SetTilesValuesForCorridors();
        SetWallValuesForLevel();
        InstantiateTiles();
        InstantiateOuterWalls();

        ShowLabel("Level " + Game.currentLevel.ToString());
        Game.SaveSession();
    }

    private void AdvanceLevel()
    {
        Game.currentLevel++;
        Game.levelComplete = false;
        ClearLevel();
        minNumRooms = (Game.currentLevel / 3) + minNumRooms;
        maxNumRooms = (Game.currentLevel * 2) + maxNumRooms;
        minRoomWidth = (Game.currentLevel / 4) + minRoomWidth;
        maxRoomWidth = (Game.currentLevel / 2) + maxRoomWidth;
        minRoomHeight = (Game.currentLevel / 4) + minRoomHeight;
        maxRoomHeight = (Game.currentLevel / 2) + maxRoomHeight;
        minCorrLength = (Game.currentLevel / 4) + minCorrLength;
        maxCorrLength = (Game.currentLevel / 2) + maxCorrLength;
        rows = Game.currentLevel % 5 == 0 ? rows + 50 : rows;
        columns = Game.currentLevel % 5 == 0 ? columns + 50 : columns;
        enemySpawnChance = enemySpawnChance < 1 ? enemySpawnChance + 0.1f : 1f;
        enemyAggressionBuff = 1.2f;
        enemyHealthBuff = 1.2f;

        foreach (var enemy in enemies)
        {
            Enemy e = enemy.GetComponent<Enemy>();
            e.aggression = e.aggression * enemyAggressionBuff;
            e.maxHealth = e.maxHealth * enemyHealthBuff;
            e.health = e.maxHealth;
        }

        LoadLevel();
    }

    public void ClearLevel()
    {
        DestroyAll("Enemy");
        DestroyAll("Environmental");
        tiles = null;
    }

    private static void DestroyAll(string tag)
    {
        var gameObjects = GameObject.FindGameObjectsWithTag(tag);

        for (var i = 0; i < gameObjects.Length; i++)
        {
            Destroy(gameObjects[i]);
        }
    }

    private void InitTiles()
    {
        tiles = new TileType[rows][];
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i] = new TileType[columns];
        }
    }

    private void CreateRoomsAndCorridors()
    {
        rooms = new Room[numRooms.Random];
        corridors = new Corridor[rooms.Length - 1];

        // Create the first room and corridor.
        rooms[0] = new Room();
        corridors[0] = new Corridor();

        // Setup the first room
        rooms[0].SetupRoom(roomWidth, roomHeight, columns, rows);

        //if player is null, spawn, otherwise check to make sure player is in a valid location
        //if (GameObject.Find("Player(Clone)") == null)
        //{
        //    player = SpawnEntity(rooms[0], player, 1f);
        //}
        if (!IsValidPos(player.transform.position))
        {
            player.transform.position = new Vector3(rooms[0].xPos + 1, rooms[0].yPos + 1);

        }

        // Setup the first corridor using the first room.
        corridors[0].SetupCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);

        for (int i = 1; i < rooms.Length; i++)
        {
            rooms[i] = new Room();

            // Setup the room based on the previous corridor.
            rooms[i].SetupRoom(roomWidth, roomHeight, columns, rows, corridors[i - 1]);

            //randomly spawn enemies
            SpawnEntity(rooms[i], enemies[0], enemySpawnChance);


            // If we haven't reached the end of the corridors array...
            if (i < corridors.Length)
            {
                // create a corridor.
                corridors[i] = new Corridor();

                // Setup the corridor based on the room that was just created.
                corridors[i].SetupCorridor(rooms[i], corridorLength, roomWidth, roomHeight, columns, rows, false);
            }
        }
    }

    private bool IsValidPos (Vector3 position)
    {
        int x = (int)position.x;
        int y = (int)position.y;

        if ( x >= 0 && x < rows && y >= 0 && y < columns)
        {
            if (tiles[x][y] == TileType.Floor)
                return true;
        }

        return false;
    }

    private void SetTilesValuesForRooms()
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            Room currentRoom = rooms[i];

            //for each room go through it's width.
            for (int j = 0; j < currentRoom.roomWidth; j++)
            {
                int xCoord = currentRoom.xPos + j;

                // For each horizontal tile, go up vertically through the room's height.
                for (int k = 0; k < currentRoom.roomHeight; k++)
                {
                    int yCoord = currentRoom.yPos + k;

                    // The coordinates array are based on the room's position and it's width and height.
                    tiles[xCoord][yCoord] = TileType.Floor;

                   
                }
            }

            //set exit in last room
            if (i == rooms.Length - 1)
            {
                tiles[currentRoom.xPos][currentRoom.yPos] = TileType.Exit;
            }
        }
    }

    private void SetTilesValuesForCorridors()
    {
        for (int i = 0; i < corridors.Length; i++)
        {
            Corridor currentCorridor = corridors[i];

            //go through it's length.
            for (int j = 0; j < currentCorridor.corridorLength; j++)
            {
                for (int k = 0; k < currentCorridor.corriderWidth; k++)
                {
                    // Start the coordinates at the start of the corridor.
                    int floorXCoord = currentCorridor.startXPos;
                    int floorYCoord = currentCorridor.startYPos;
                    int wallXCoord = currentCorridor.startYPos;
                    int wallYCoord = currentCorridor.startYPos;

                    // Depending on the direction, add or subtract from the appropriate
                    // coordinate based on how far through the length the loop is.
                    switch (currentCorridor.direction)
                    {
                        case Direction.North:
                            floorXCoord += k;
                            floorYCoord += j;
                            break;
                        case Direction.East:
                            floorXCoord += j;
                            floorYCoord += k;
                            break;
                        case Direction.South:
                            floorXCoord += k;
                            floorYCoord -= j;
                            break;
                        case Direction.West:
                            floorXCoord -= j;
                            floorYCoord += k;
                            break;
                    }
                    floorXCoord = (int) Mathf.Clamp(floorXCoord, 0f, tiles.Length - 1);
                    floorYCoord = (int)Mathf.Clamp(floorYCoord, 0f, tiles[0].Length - 1);


                    if (tiles[floorXCoord][floorYCoord] != TileType.Exit)
                        tiles[floorXCoord][floorYCoord] = TileType.Floor;

                }

            }
        }
    }

    private void SetWallValuesForLevel()
    {
        int n = tiles.Length;
        for( int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if ( tiles[i][j] == TileType.Floor || tiles[i][j] == TileType.Exit)
                {
                    int[] x = { i - 1, i, i + 1 };
                    int[] y = { j - 1, j, j + 1 };
                    foreach (int k in x )
                    {
                        foreach (int l in y)
                        {
                            if (k >= 0 && k < n && l >= 0 && l < n)
                            {
                                if (tiles[k][l] != TileType.Floor && tiles[k][l] != TileType.Exit)
                                    tiles[k][l] = TileType.Wall;
                            }
                        }
                    }
                }
            }
        }
    }

    private void InstantiateTiles()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[i].Length; j++)
            {
                if (tiles[i][j] == TileType.Wall)
                    InstantiateFromArray(wallTiles, i, j);
                else if (tiles[i][j] == TileType.Exit)
                {
                    InstantiateFromArray(exitTiles, i, j);
                }
                else if (tiles[i][j] == TileType.Floor)
                {
                    InstantiateFromArray(floorTiles, i, j);
                }
                else
                    InstantiateFromArray(backgroundTiles, i, j);
            }
        }
    }

    private void InstantiateOuterWalls()
    {
        // The outer walls are one unit left, right, up and down from the board.
        float leftEdgeX = -1f;
        float rightEdgeX = columns + 0f;
        float bottomEdgeY = -1f;
        float topEdgeY = rows + 0f;

        InstantiateVerticalOuterWall(leftEdgeX, bottomEdgeY, topEdgeY);
        InstantiateVerticalOuterWall(rightEdgeX, bottomEdgeY, topEdgeY);
        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, bottomEdgeY);
        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, topEdgeY);
    }

    private void InstantiateVerticalOuterWall(float xCoord, float startingY, float endingY)
    {
        // Start the loop at the starting value for Y.
        float currentY = startingY;

        // While the value for Y is less than the end value...
        while (currentY <= endingY)
        {
            // ... instantiate an outer wall tile at the x coordinate and the current y coordinate.
            InstantiateFromArray(outerWallTiles, xCoord, currentY);

            currentY++;
        }
    }

    private void InstantiateHorizontalOuterWall(float startingX, float endingX, float yCoord)
    {
        // Start the loop at the starting value for X.
        float currentX = startingX;

        // While the value for X is less than the end value...
        while (currentX <= endingX)
        {
            // ... instantiate an outer wall tile at the y coordinate and the current x coordinate.
            InstantiateFromArray(outerWallTiles, currentX, yCoord);

            currentX++;
        }
    }

    private void InstantiateFromArray(GameObject[] prefabs, float xCoord, float yCoord)
    {
        // Create a random index for the array.
        int randomIndex = UnityEngine.Random.Range(0, prefabs.Length);

        // The position to be instantiated at is based on the coordinates.
        Vector3 position = new Vector3(xCoord, yCoord, 0f);

        // Create an instance of the prefab from the random index of the array.
        GameObject tileInstance = Instantiate(prefabs[randomIndex], position, Quaternion.identity) as GameObject;

        // Set the tile's parent to the environment
        tileInstance.transform.parent = transform;
    }

    private GameObject SpawnEntity(Room room, GameObject obj, float spawnChance)
    {
        int x = Mathf.Abs(room.xPos);
        int y = Mathf.Abs(room.yPos);
        IntRange xRange = new IntRange(0, room.roomWidth);
        IntRange yRange = new IntRange(0, room.roomHeight);
        float rand = UnityEngine.Random.value;

        if (rand < spawnChance)
            return Instantiate(obj, new Vector3(room.xPos + xRange.Random, y + yRange.Random), Quaternion.identity) as GameObject;

        return null;
    }

    public static List<Vector3> GetPath(Vector3 origin, Vector3 target)
    {
          
        //List<Tile> open = new List<Tile>();
        //List<Tile> closed = new List<Tile>();
        //Tile source = getNearByValidTile(origin);
        //source.g = 0;
        //open.Add(source);

        //Tile destination = getNearByValidTile(target);

        //Tile current;
        //while (open.Any())
        //{
        //    current = open.Aggregate((accumulator, tile) => accumulator.f < tile.f ? accumulator : tile);
        //    float x = current.transform.position.x;//current.transform.position.x > 0 ? Mathf.Floor(current.transform.position.x) : Mathf.Ceil(current.transform.position.x);
        //    float y = current.transform.position.y;//current.transform.position.y > 0 ? Mathf.Floor(current.transform.position.y) : Mathf.Ceil(current.transform.position.y);

        //    open.Remove(current);
        //    closed.Add(current);

        //    //FOUND TARGET
        //    if (current.transform.position == destination.transform.position)
        //    {
        //        Tile thisParent = destination;
        //        List<Vector3> path = new List<Vector3>();
        //        while (thisParent.transform.position != source.transform.position)
        //        {
        //            path.Add(thisParent.transform.position);
        //            thisParent = thisParent.parent;
        //        }
        //        path = path.Reverse<Vector3>().ToList();
        //        Time.timeScale = 1f;
        //        return path;
        //    }

        //    var coords = new List<Point> {
        //        new Point( x, y - 1f ),
        //        new Point( x, y + 1f ),
        //        new Point( x - 1f, y ),
        //        new Point( x + 1f, y )
        //        };

        //    //CHECK EACH ORTHOGONAL DIRECTION
        //    foreach (Point coord in coords)
        //    {
        //        if (nodes.ContainsKey(coord))
        //        {
        //            var node = nodes[coord];
        //            if (!closed.Contains(node) && node.isWalkable)
        //            {
        //                if (open.Contains(node))
        //                {
        //                    if (current.g < node.g)
        //                    {
        //                        node.parent = current;
        //                        node.RecalculateG();
        //                    }
        //                }
        //                else
        //                {
        //                    node.h = (int)Mathf.Abs(node.transform.position.x - target.x) + (int)Mathf.Abs(node.transform.position.y - target.y);
        //                    node.parent = current;
        //                    node.g = node.parent.g + 10;
        //                    Debug.DrawLine(node.transform.position, source.transform.position);
        //                    open.Add(node);
        //                }
        //            }
        //        }
        //    }
        //}

        return new List<Vector3>();
    }

    private static Tile getNearByValidTile(Vector3 origin)
    {
        float x = origin.x > 0 ? Mathf.Floor(origin.x) : Mathf.Ceil(origin.x);
        float y = origin.y > 0 ? Mathf.Floor(origin.y) : Mathf.Ceil(origin.y);
        Point point = new Point(x, y);
        Tile result = nodes[point];

        if (!isValidTile(point))
        {
            foreach (Point p in new List<Point> {new Point(x, y + 1), new Point(x, y - 1),
                                                    new Point(x - 1, y), new Point(x + 1, y)})
            {
                if (isValidTile(point))
                {
                    result = nodes[p];
                }
            }
        }

        return result;
    }

    private static bool isValidTile(Point point)
    {
        if (nodes.ContainsKey(point))
        {
            if (nodes[point].isWalkable)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            throw new Exception("Point " + point + " not present in dictionary.");

       // return false;
    }

    private void ShowLabel(string text)
    {
        labelText = text;
        showLevelLabel = true;
        displayTimePassed = false;
        fadeTimePassed = false;
        elapsedTime = 0;
    }

    void OnGUI()
    {
        if ( showLevelLabel )
        {
            float w = 0.3f;
            float h = 0.2f;
            Rect rect = new Rect((Screen.width * (1 - w)) / 2, ((Screen.height * (1 - h)) / 2) - 50, Screen.width * w, Screen.height * h);
            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;
            style.fontSize = 25;
            if ( Mathf.Approximately(labelAlpha, 1f))
            {
                GUI.Label(rect, labelText, style);
            }
            else
            {
                Color tmp = GUI.color;
                GUI.color =  new Color(1,1,1,labelAlpha);
                GUI.Label(rect, labelText, style);
                GUI.color = tmp;
            }
        }
    }
}

