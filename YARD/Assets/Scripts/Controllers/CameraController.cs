﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
class CameraController : MonoBehaviour
{
    public float zoomedInLevel = 10;
    public float zoomedOutLevel = 50;
    public float zoomSpeed = 0.25f;
    public float zoomTolerance = 1;

    public bool zoomingIn = false;
    public bool zoomingOut = false;

    public Transform player;

    Camera camera;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        camera = GetComponent<Camera>();
    }

    void Update()
    {
        transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);

        if (zoomingIn)
        {
            camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, zoomedInLevel, zoomSpeed * Time.deltaTime);
            if (camera.orthographicSize < zoomedInLevel + zoomTolerance)
            {
                camera.orthographicSize = zoomedInLevel;
                zoomingIn = false;
            }
        }
        if (zoomingOut)
        {
            camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, zoomedOutLevel, zoomSpeed * Time.deltaTime);
            if (camera.orthographicSize > zoomedOutLevel - zoomTolerance)
            {
                camera.orthographicSize = zoomedOutLevel;
                zoomingOut = false;
            }
        }
    }

    public void ZoomInCamera()
    {
        zoomingIn = true;
        zoomingOut = false;
    }

    public void ZoomOutCamera()
    {
        zoomingOut = true;
        zoomingIn = false;
    }
}