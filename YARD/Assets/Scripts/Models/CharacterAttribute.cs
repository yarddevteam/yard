﻿[System.Serializable]
public enum CharacterAttribute
{
    Health,
    Mana,
    Stamina,
    MaxHealth,
    MaxMana,
    MaxStamina,
    Dodge,
    Defense,
    Speed
}
