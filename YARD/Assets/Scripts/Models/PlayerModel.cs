﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerModel : CharacterModel
{
    public PlayerProfile profile;
    public MeleeWeapon meleeWeapon;
    public RangedWeapon rangedWeapon;
    public Weapon shield;
    public Item headEquipment;
    public Item chestEquipment;
    public Item legsEquipment;
    public Item footEquipment;
    public List<Item> inventory = new List<Item>();
    public float staminaUseageRate = 8f;
    public float staminaRegenRate = 1.5f;
    public float healthRegenRate = 1.1f;
    public float manaRegenRate = 0.7f;

}

