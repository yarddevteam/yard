﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;
    public AudioSource soundTracks;
    public AudioSource soundEffects;

    //Sound Track Constants
    public const string ST_MAIN_MENU = "ST_MAIN_MENU";
    public const string ST_COMBAT = "ST_COMBAT";
    public const string ST_BOSS_FIGHT = "ST_BOSS_FIGHT";
    public const string ST_END_OF_FIGHT = "ST_END_OF_FIGHT";
    public const string ST_LEVEL = "ST_LEVEL";
    //Sound Effects Constants
    public const string SE_ATTACK = "SE_ATTACK";
    public const string SE_CAST = "SE_CAST";
    public const string SE_DEFEND = "SE_DEFEND";
    //Sound Tracks
    public AudioClip[] mainMenuAudioClips;
    public AudioClip[] combatAudioClips;
    public AudioClip[] bossFightAudioClips;
    public AudioClip[] endOfFightAudioClips;
    public AudioClip[] levelAudioClips;
    //Sound Effects
    public AudioClip[] attackSoundClips;
    public AudioClip[] castSoundClips;
    public AudioClip[] defendSoudnClips;

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            //Destroy(gameObject);
        }
    }
    void Start() 
    {
        //soundTracks = Camera.main.GetComponent<AudioSource>();
        //soundEffects = Camera.main.GetComponentInChildren<AudioSource>();

        //playSoundTrack(ST_MAIN_MENU);
    }

    void Update()
    {
    }

    public void playSoundTrack(string type)
    {
        switch (type)
        {
            case ST_MAIN_MENU:
                playSound(mainMenuAudioClips);
                break;
            case ST_COMBAT:
                playSound(combatAudioClips);
                break;
            case ST_BOSS_FIGHT:
                playSound(bossFightAudioClips);
                break;
            case ST_END_OF_FIGHT:
                playSound(endOfFightAudioClips);
                break;
            case ST_LEVEL:
                playSound(levelAudioClips);
                break;
        }
    }

    public void playSoundTrack(string type, int clip)
    {
        switch (type)
        {
            case ST_MAIN_MENU:
                playSound(mainMenuAudioClips, clip);
                break;
            case ST_COMBAT:
                playSound(combatAudioClips, clip);
                break;
            case ST_BOSS_FIGHT:
                playSound(bossFightAudioClips, clip);
                break;
            case ST_END_OF_FIGHT:
                playSound(endOfFightAudioClips, clip);
                break;
        }
    }

    public void playSoundEffect(string type)
    {
        switch (type)
        {
            case SE_ATTACK:
                playEffect(attackSoundClips);
                break;
            case SE_CAST:
                playEffect(castSoundClips);
                break;
            case SE_DEFEND:
                playEffect(defendSoudnClips);
                break;
        }
    }

    public void playSoundEffect(string type, int clip)
    {
        switch (type)
        {
            case SE_ATTACK:
                playEffect(attackSoundClips, clip);
                break;
            case SE_CAST:
                playEffect(castSoundClips, clip);
                break;
            case SE_DEFEND:
                playEffect(defendSoudnClips, clip);
                break;
        }
    }

    private void playSound(AudioClip[] clipType)
    {
        soundTracks.clip = clipType[Random.Range(0, clipType.Length)];
        soundTracks.Play();
    }

    private void playSound(AudioClip[] clipType, int clip)
    {
        soundTracks.clip = clipType[clip];
        soundTracks.Play();
    }

    private void playEffect(AudioClip[] clipType)
    {
        soundEffects.clip = clipType[Random.Range(0, clipType.Length)];
        soundEffects.Play();
    }

    private void playEffect(AudioClip[] clipType, int clip)
    {
        soundEffects.clip = clipType[clip];
        soundEffects.Play();
    }

}