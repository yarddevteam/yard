﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public Weapon weapon;
    public float speed;
    public float damage;
    public float knockback;
    public GameObject text;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Vector2 direction = (other.transform.position - transform.position).normalized;

        if (other.gameObject.tag == "Enemy")
        {
            EnemyController victim = other.GetComponent<EnemyController>();
            victim.TakeDamage(damage, knockback * direction);
            Vector3 v = Camera.main.WorldToViewportPoint(other.transform.position);
           // DmgPoints.SpawnText(weapon.damage, v.x, v.y, text);

            if (!victim.model.isAlive)
            {
                weapon.OnWeaponKill();
            }

            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Environmental")
           if( other.gameObject.GetComponent<Tile>().type == Environment.TileType.Wall)
                Destroy(gameObject);
        
    }
}
