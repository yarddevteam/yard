﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad
{
    public static Game savedGame;

    public static void Save(Game currentGame)
    {
        savedGame = Game.currentGame;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGame.gd");
        bf.Serialize(file, savedGame);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGame.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGame.gd", FileMode.Open);
            savedGame = (Game)bf.Deserialize(file);
            file.Close();
        }
    }

    public static void SaveProfileList()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/profiles.gd");
        bf.Serialize(file, Game.currentGame.profiles);
        file.Close();
    }

    public static void LoadProfileList()
    {
        if (File.Exists(Application.persistentDataPath + "/profiles.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/profiles.gd", FileMode.Open);
            Game.currentGame.profiles = (List<PlayerProfile>) bf.Deserialize(file);
            file.Close();
        }
    }
}