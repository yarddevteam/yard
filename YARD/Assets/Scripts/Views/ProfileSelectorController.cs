﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using System;

public class ProfileSelectorController : MonoBehaviour {
    public Dropdown profileDropDown;
    public InputField input;

    // Use this for initialization
    void Start () {
        //profileDropDown = GetComponent<Dropdown>();
        profileDropDown.options.Clear();

        //input = gameObject.GetComponent<InputField>();
        var se = new InputField.SubmitEvent();
        se.AddListener(SubmitName);
        input.onEndEdit = se; 

        foreach (PlayerProfile p in Game.currentGame.profiles)
        {
            profileDropDown.options.Add(new Dropdown.OptionData() { text = p.name });
        }

        //force view to refresh
        profileDropDown.value = 1;
        profileDropDown.value = 0;
    }

    public void OnNewProfile()
    {
        profileDropDown.gameObject.SetActive(false);
        input.gameObject.SetActive(true);
    }

    private void SubmitName(string name)
    {
        if (Input.GetButtonDown("Submit"))
        {
            PlayerProfile p = new PlayerProfile(name, Game.currentGame.profiles.Count);
            Game.currentGame.profiles.Add(p);
            SaveLoad.SaveProfileList();
            Game.currentGame.selectedProfile = p;
            SceneManager.LoadScene(1);
        }
    }

    public void OnLoadProfile()
    {
        List<PlayerProfile> profiles = Game.currentGame.profiles;
        foreach (PlayerProfile p in profiles)
        {
            if ( p.name == profileDropDown.captionText.text)
            {
                Game.currentGame.selectedProfile = p;
                SceneManager.LoadScene(1);
                break;
            }
        }
    }

    public void OnQuit()
    {
        Application.Quit();
    }
}

