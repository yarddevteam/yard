﻿using System;
using UnityEngine;

[Serializable]
public class CharacterMotionState
{
    public enum mState { RIGHT, LEFT, UP, DOWN, IDLE };
    public mState currentState = mState.IDLE;
    public bool isMoving = false;

    public void Update(Vector3 v)
    {
        if (v.x > 0)
        {
            //moving right
            isMoving = true;
            currentState = mState.RIGHT;
        }
        else if (v.x < 0)
        {
            //move left
            isMoving = true;
            currentState = mState.LEFT;
        }
        else if (v.y > 0)
        {
            //move up
            isMoving = true;
            currentState = mState.UP;
        }
        else if (v.y < 0)
        {
            //move down
            isMoving = true;
            currentState = mState.DOWN;
        }
        else
        {
            //idle
            isMoving = false;
            currentState = mState.IDLE;
        }
    }
}