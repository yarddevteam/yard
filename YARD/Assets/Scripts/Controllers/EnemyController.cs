﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Devdog.InventorySystem.UI;
using Devdog.InventorySystem;

[Serializable]
public class EnemyController : CharController, IObjectTriggerUser
{
    public Enemy model;
    public Transform player;
    public bool canSeePlayer;
    public Seeker seeker;
    private int maxSeeAhead = 2;
    public float detecterRange = 1f;
    private Vector3 steering;
    public float avoidanceConstant = 2f;
    public float detecterOffset = 0.75f;
    public InventoryItemBase[] item;

    List<InventoryItemBase> itemPickups;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        seeker = GetComponent<Seeker>();
        itemPickups = item.ToList();
    }

    void Update()
    {
        if ( !player)
            player = GameObject.Find("Player(Clone)").transform;

        if (model.health <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        Color color = GetComponent<SpriteRenderer>().color;

        if ((CanDetectPlayer() /*&& Vector3.Distance(transform.position, player.position) > model.minSeekRange)*/ || seeker.isSeeking))
        {
            if (alpha < 1 )
            {
                alpha += Time.deltaTime / 0.2f;
                GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, alpha);
            }
            steering = Vector3.zero;
            steering += SeekPlayer();
            steering += CollisionAvoidance(steering);
            //Debug.DrawRay(transform.position, steering * model.speed * model.aggression * Time.fixedDeltaTime, Color.green, 0.35f);
            model.velocity = steering * model.speed * model.aggression * Time.fixedDeltaTime;
            MoveTo(model.velocity);
        }
        else
        {
            if (alpha > 0)
                alpha -= Time.deltaTime / 0.5f;

            GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, alpha);
        }
            

        //Debug.Log(canSeePlayer);
        if (hasReachedPlayer())
        {
            seeker.Reset();
            //Debug.Log("Has Reached Player.");
            StartAttack();
            return;
        }

    }

    private Vector3 CollisionAvoidance(Vector3 velocity)
    {
        //Detech any obstacle
        Vector3 displacement;
        float avoidanceForce;
        Vector3 verticalOffset = new Vector3(0, (transform.GetComponent<Collider2D>().transform.position.y) * detecterOffset);
        Vector3 horizontalOffset = new Vector3((transform.GetComponent<Collider2D>().transform.position.x) * detecterOffset, 0);
        Vector3 topDetecterRight = transform.position + verticalOffset + horizontalOffset;
        Vector3 topDetecterLeft = transform.position + verticalOffset - horizontalOffset;
        Vector3 bottomDetecterRight = transform.position - verticalOffset + horizontalOffset;
        Vector3 bottomDetecterLeft = transform.position - verticalOffset - horizontalOffset;

        List<RaycastHit2D> collisionDetecters = new List<RaycastHit2D>
        {
            Physics2D.Raycast(topDetecterRight, velocity, detecterRange),
            Physics2D.Raycast(topDetecterLeft, velocity, detecterRange),
            Physics2D.Raycast(bottomDetecterRight, velocity, detecterRange),
            Physics2D.Raycast(bottomDetecterLeft, velocity, detecterRange)
        };

        Debug.DrawRay(topDetecterRight, velocity * detecterRange, Color.blue);
        Debug.DrawRay(topDetecterLeft, velocity * detecterRange, Color.blue);
        Debug.DrawRay(bottomDetecterRight, velocity * detecterRange, Color.blue);
        Debug.DrawRay(bottomDetecterLeft, velocity * detecterRange, Color.blue);

        foreach (RaycastHit2D detecter in collisionDetecters)
        {
            if (detecter.collider != null)
            {
                if (detecter.collider.gameObject.CompareTag("Obstacles"))
                {
                    displacement = transform.position - detecter.collider.bounds.center;
                    avoidanceForce = avoidanceConstant * velocity.magnitude * detecter.transform.GetComponent<BoxCollider2D>().size.magnitude / (float)Math.Pow(displacement.magnitude, 2);
                    velocity += avoidanceForce * displacement;
                    Debug.DrawRay(detecter.collider.bounds.center, (int)avoidanceForce * displacement, Color.white, 1f);
                }
            }
        }

        return velocity.normalized;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        GameObject other = coll.gameObject;
        if (other.tag == "Player")
        {
            Vector2 direction = (other.transform.position - transform.position).normalized;
            Vector2 force = direction * model.knockback;
            other.GetComponent<PlayerController>().TakeDamage(model.damage, force);
            Debug.DrawRay(transform.position, -force);
            model.rigidBody2D.AddForce(-force);
        }
    }

    private bool CanDetectPlayer()
    {
        Vector3 displacement = player.position - transform.position;

        //check if the enemy can see the player first
        if (Vector3.Distance(transform.position, player.position) <= model.maxSeekRange)
        {

            Vector3 colliderOffset = transform.position + displacement.normalized;
            RaycastHit2D hit = Physics2D.Raycast(colliderOffset, displacement.normalized);
            //Debug.DrawRay(colliderOffset, displacement, Color.magenta, 2f);

            if (hit.collider != null)
            {
                if (hit.transform == player)
                {
                    canSeePlayer = true;
                    seeker.lastTargetPos = player.position;
                    return true;
                }
                else
                    canSeePlayer = false;
            }
        }

        //then check if the enemy is sense the player
        if (model.IsPerceptive)
        {
            if (displacement.magnitude <= model.senseRadius)
            {
                seeker.lastTargetPos = player.position;
                return true;
            }
        }

        return false;
    }

    private Vector3 SeekPlayer()
    {
        if (canSeePlayer)
        {
            Vector3 displacementDirection = (player.position - transform.position).normalized;
            //MoveTo(displacementDirection * model.speed * model.aggression * Time.fixedDeltaTime);
            seeker.Reset();
            seeker.isSeeking = true;
            Debug.DrawRay(transform.position, displacementDirection, Color.cyan);
            return displacementDirection;
        }
        else
        {
            //First time enemy has detected player, start pathfinding
            if (seeker.path.Count == 0 && seeker.lastTargetPos != null)
            {
                seeker.StartPath(transform.position);
            }

            //Path is already set and enemy is currently seeking. 
            //Vector3 direction = (seeker.path[seeker.currentWaypoint] - transform.position).normalized;
            //Debug.DrawRay(transform.position, direction, Color.red);
            //MoveTo(direction * model.speed * Time.fixedDeltaTime);

            //Debug.Log(seeker.currentWaypoint);

            //if (Vector3.Distance(transform.position, seeker.path[seeker.currentWaypoint]) < seeker.nextWaypointDistance)
            //{
            //    seeker.currentWaypoint++;
            //}

            return Vector3.zero;
        }
    }

    private bool hasReachedPlayer()
    {
        //TODO: 1f needs to be changed to a property of the enemy
        return (transform.position - player.transform.position).magnitude < 0.5f || seeker.currentWaypoint >= seeker.path.Count;
    }

    public override void TakeDamage(float damage, Vector2 knockback)
    {
        model.rigidBody2D.AddForce(knockback);
        model.health -= damage;
        if (model.health <= 0)
        {
            model.isAlive = false;
            var currPosition = gameObject.transform.position;
            SpawnFromBody(currPosition);
        }
    }

    public void SpawnFromBody(Vector3 pos)
    {
        int num = UnityEngine.Random.Range(0, itemPickups.Count);
        if (itemPickups[num].rarity.ID <= Game.currentLevel)
        {
            GameObject barrelPU = Instantiate(itemPickups[num].gameObject, pos, Quaternion.identity) as GameObject;
        }
    }
}