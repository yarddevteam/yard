﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RangedWeapon : Weapon
{
    public GameObject projectile;
    public float manaCost = 2f;
    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").gameObject;
    }

    public override void UseInDirctionOfMouseClick()
    {
        Vector3 screenMousePos = Input.mousePosition;
        screenMousePos.z = 1;
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(screenMousePos);
        Vector3 mouseDisplacement = mousePos - player.transform.position;
        GameObject p = (GameObject)Instantiate(projectile, player.transform.position , Quaternion.identity);
        p.GetComponent<Projectile>().weapon = this;
        p.GetComponent<Projectile>().speed = speed;
        p.GetComponent<Projectile>().damage = damage;
        p.GetComponent<Projectile>().knockback = knockback;
        Vector3 force = mouseDisplacement.normalized * p.GetComponent<Projectile>().speed;
        p.GetComponent<Rigidbody2D>().AddForce(force);
        AudioController.instance.playSoundEffect(AudioController.SE_CAST);
    }
}

