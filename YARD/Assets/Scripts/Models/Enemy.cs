﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class Enemy: CharacterModel
{
    public GameObject target;
    public int damage = 1;
    public float knockback = 10f;
    public int defaultMaxHealth = 10;
    public float defaultAggression = 1f;
    public float aggression = 1f;
    public float minSeekRange = 2f;
    public float maxSeekRange = 20f;
    public float seekMultiplier = 5f;
    public float senseRadius = 0f;
    public bool isPerceptive = false;
    public AudioSource attacksound;
    public bool isAlive = true;

    void Start()
    {
    }

    internal bool IsPerceptive {
        get
        {
            return isPerceptive;
        }
        set
        {
            if (senseRadius > 0)
                isPerceptive = value;
        }
    }

    internal void Reset()
    {
        maxHealth = defaultMaxHealth;
        aggression = defaultAggression;
    }
}
