﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Stat {
	
	[SerializeField]
	public HudBar bar;

	[SerializeField]
	private float currentVal;
	public float CurrentVal
    {	
		get
        {
			return currentVal;
		}

		set
        {
			currentVal = Mathf.Clamp (value, 0, maxVal);
			bar.Value = currentVal;
		}
	}

	[SerializeField]
	private float maxVal;
	public float MaxVal
    {
		get
        {
			return maxVal;
		}

		set
        {
			maxVal = value;
			bar.MaxValue = value;
		}
	}

    public float regenRate;
    public float usageRate;

    internal void Init(float max, float regen)
    {
        MaxVal = max;
        CurrentVal = max;
        regenRate = regen;
    }

    internal void Init(float max, float regen, float useage)
    {
        MaxVal = max;
        CurrentVal = max;
        regenRate = regen;
        usageRate = useage;
    }

    internal void Regen()
    {
        if (CurrentVal < MaxVal)
        {
            CurrentVal += regenRate * Time.deltaTime;
        }
    }
}
