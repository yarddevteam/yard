﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Point : IEnumerable<float>
{
    public readonly float x, y;
    List<float> coords = new List<float>();

    public Point(float x, float y)
    {
        this.x = x;
        this.y = y;
        coords.Add(x);
        coords.Add(y);
    }

    public override bool Equals(System.Object obj)
    {
        // If parameter is null return false.
        if (obj == null)
        {
            return false;
        }

        // If parameter cannot be cast to Point return false.
        Point p = obj as Point;
        if ((System.Object)p == null)
        {
            return false;
        }

        // Return true if the fields match:
        return (x == p.x) && (y == p.y);
    }

    public bool Equals(Point p)
    {
        // If parameter is null return false:
        if ((object)p == null)
        {
            return false;
        }

        // Return true if the fields match:
        return (x == p.x) && (y == p.y);
    }

    public override int GetHashCode()
    {
        return (int) Math.Floor(x) ^ (int) Math.Floor(y);
    }



    public static bool operator ==(Point a, Point b)
    {
        // If both are null, or both are same instance, return true.
        if (System.Object.ReferenceEquals(a, b))
        {
            return true;
        }

        // If one is null, but not both, return false.
        if (((object)a == null) || ((object)b == null))
        {
            return false;
        }

        // Return true if the fields match:
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Point a, Point b)
    {
        return !(a == b);
    }

    public override string ToString()
    {
        return "( " + x + ", " + y + " )";
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        foreach (float x in this.coords)
        {
            yield return x;
        }
    }

    IEnumerator<float> IEnumerable<float>.GetEnumerator()
    {
        return ((IEnumerable<float>)coords).GetEnumerator();
    }
}

